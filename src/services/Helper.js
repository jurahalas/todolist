
export function getTasksByFilter (tasks, filter) {
  switch(filter) {
    case 'all':
      return tasks;
    case 'completed':
      return tasks.filter(task => task.status === 'completed');
    case 'active':
      return tasks.filter(task => task.status === 'active');
    default:
      throw new Error(`Unknown filter: ${ filter}`);
  }
}