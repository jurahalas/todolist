import React from 'react';
import './App.css';
import TodoList from '../todoList/TodoList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as todoListActions from '../todoList/TodoListActions';
import AddTodo from '../components/addTodo/AddTodo';
import PropTypes from 'prop-types';
import Footer from '../components/footer/Footer';

const App = ({ todoListActions, todoListReducer: { filter } }) => {
  const createNewTask = (task) =>
    todoListActions.createTask(task);

  const onToDoFilter = (filter) =>
    todoListActions.setFilter(filter);

  return (
    <div className='to-do-app'>
      <AddTodo
        onToDoAdd={ createNewTask }
      />
      <TodoList />
      <Footer
        currentFilter={ filter }
        onToDoFilter={ onToDoFilter }
      />
    </div>
  );
};

App.propTypes = {
  todoListReducer: PropTypes.object.isRequired,
  todoListActions: PropTypes.object.isRequired,
};

function mapStateToProps(state ) {
  return {
    todoListReducer: state.todoListReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    todoListActions: bindActionCreators(todoListActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
