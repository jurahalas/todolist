import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducersFactory from '../../reducersFactory';

const store = createStore(reducersFactory);

it('renders without crashing', () => {
  ReactDOM.render(
    <Provider store={ store }>
      <App
        todoListActions={ {
          setFilter: jest.fn(),
        } }
        todoListReducer={ {
          filter: '',
        } }
      />
    </Provider>, document.createElement('div'));
});