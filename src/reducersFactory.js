import { combineReducers } from 'redux';
import todoListReducer from './todoList/TodoListReducer';

export default combineReducers({
  todoListReducer,
});