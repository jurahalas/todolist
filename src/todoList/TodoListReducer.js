import { todoListActionsTypes } from './TodoListConstants';

const initialState = {
  tasks: [],
  filter: 'all',
  nextTodoId: 0,
};

export default function todoListReducer(state = initialState, action) {
  const { type, payload } = action;

  switch(type) {
    case todoListActionsTypes.CREATE_TASK:
      const { text } = payload;

      return {
        ...state,
        nextTodoId: state.nextTodoId + 1,
        filter: 'all',
        tasks: [
          {
            id: state.nextTodoId,
            text,
            status: 'active',
          },
          ...state.tasks,
        ],
      };

    case todoListActionsTypes.SET_TASK_STATUS:
      const tasks = state.tasks.map(task =>
        (task.id === payload.id)
          ? {...task, status: task.status === 'active' ? 'completed' : 'active'}
          : task
      );

      return {
        ...state,
        tasks: tasks,
      };

    case todoListActionsTypes.SET_FILTER:
      return {
        ...state,
        filter: payload.filter,
      };

    case todoListActionsTypes.DELETE_TASK:
      const updatedTodos = state.tasks.filter(task =>
        (task.id !== payload.id)
      );

      return {
        ...state,
        tasks: updatedTodos,
        filter: 'all',
      };

    default:
      return state;
  }
}