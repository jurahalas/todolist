import React from 'react';
import PropTypes from 'prop-types';
import './TodoList.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TodoItem from '../components/todoItem/TodoItem';
import * as todoListActions from './TodoListActions';
import { getTasksByFilter } from '../services/Helper';

const TodoList = ({ todoListReducer: { tasks, filter }, todoListActions }) => {
  const todos = getTasksByFilter(tasks, filter);

  return (
    <div className='todo-list-container'>
      { todos.map(({ id, text, status }) =>
        <TodoItem
          key={ id }
          text={ text }
          completed={ status === 'completed' }
          onToDoAccept={ () => todoListActions.setTaskStatus(id) }
          onDelete={ () => todoListActions.deleteTask(id) }
        />
      )
      }
    </div>
  );
};

TodoList.propTypes = {
  todoListReducer: PropTypes.object.isRequired,
  todoListActions: PropTypes.object.isRequired,
};

function mapStateToProps(state ) {
  return {
    todoListReducer: state.todoListReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    todoListActions: bindActionCreators(todoListActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);