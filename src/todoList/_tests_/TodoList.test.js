import React from 'react';
import ReactDOM from 'react-dom';
import TodoList from '../TodoList';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducersFactory from '../../reducersFactory';

const store = createStore(reducersFactory);

it('renders without crashing', () => {
  ReactDOM.render(
    <Provider store={ store }>
      <TodoList
        todoListActions={ {
          setTaskStatus: jest.fn(),
          deleteTask: jest.fn(),
        } }
        todoListReducer={ {
          tasks: '',
          filter: '',
        } }
      />
    </Provider>, document.createElement('div'));
});