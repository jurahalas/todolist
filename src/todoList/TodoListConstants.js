import keyMirror from 'keymirror';

export const todoListActionsTypes = keyMirror(
  {
    CREATE_TASK: null,
    SET_TASK_STATUS: null,
    SET_FILTER: null,
    DELETE_TASK: null,
  }
);