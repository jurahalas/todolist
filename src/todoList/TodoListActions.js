import { todoListActionsTypes } from './TodoListConstants';

export function createTask(text) {
  return {
    type: todoListActionsTypes.CREATE_TASK,
    payload: {
      text,
    },
  };
}

export function setTaskStatus(id) {
  return {
    type: todoListActionsTypes.SET_TASK_STATUS,
    payload: {
      id,
    },
  };
}

export function deleteTask(id) {
  return {
    type: todoListActionsTypes.DELETE_TASK,
    payload: {
      id,
    },
  };
}

export function setFilter(filter) {
  return {
    type: todoListActionsTypes.SET_FILTER,
    payload: {
      filter,
    },
  };
}