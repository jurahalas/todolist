import React from 'react';
import './TodoItem.css';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import checkboxChecked from '../../images/check-active.svg';
import checkboxUnchecked from '../../images/check-inactive.svg';

const TodoItem = ({ text, completed, onToDoAccept, onDelete }) => (
  <div className={ classNames('to-do-item-container', { 'done': completed}) } >
    <img
      alt=''
      className='checkbox'
      onClick={ onToDoAccept }
      src={completed ? checkboxChecked : checkboxUnchecked }
    />
    <div className="text">
      { text }
    </div>
    <div
      className="delete-task"
      onClick={ onDelete }
    > ×
    </div>
  </div>
);

TodoItem.propTypes = {
  text: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
  onToDoAccept: PropTypes.func,
  onDelete: PropTypes.func,
};
export default TodoItem;