import React from 'react';
import ReactDOM from 'react-dom';
import TodoItem from '../TodoItem';

it('renders without crashing', () => {
  ReactDOM.render(
    <TodoItem
      text={ '' }
      completed={ true }
      onToDoAccept={ jest.fn() }
      onDelete={ jest.fn() }
    />, document.createElement('div'));
});