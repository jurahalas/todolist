import React from 'react';
import ReactDOM from 'react-dom';
import Footer from '../Footer';

it('renders without crashing', () => {
  ReactDOM.render(
    <Footer
      onToDoFilter={ jest.fn() }
      currentFilter={ 'active' }
    />, document.createElement('div'));
});