import React from 'react';
import PropTypes from 'prop-types';
import './Footer.css';

const Footer = ({ onToDoFilter, currentFilter }) => {
  const setFilter = (filter) => onToDoFilter(filter);

  return (
    <div className='footer-container'>
      <div
        className={ currentFilter === 'all' ? 'active' : '' }
        onClick={ () => setFilter('all') }
      >
        All
      </div>
      <div
        className={ currentFilter === 'active' ? 'active' : '' }
        onClick={ () => setFilter('active') }
      >
        Active
      </div>
      <div
        className={ currentFilter === 'completed' ? 'active' : '' }
        onClick={ () => setFilter('completed') }
      >
        Completed
      </div>
    </div>
  );
};


Footer.propTypes = {
  currentFilter: PropTypes.string.isRequired,
  onToDoFilter: PropTypes.func.isRequired,
};

export default Footer;