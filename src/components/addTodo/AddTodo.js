import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './AddTodo.css';

class AddTodo extends Component {
  constructor(){
    super();

    this.state = {
      text: '',
    };
  }

  handleChangeInput=(e) => {
    this.setState({text: e.target.value});
  };

  handleKeyPressInput=(e) => {
    const value = e.target.value.replace(/\s{2,}/g, ' ');

    if(e.key === 'Enter' && value) {
      this.props.onToDoAdd(value);
      this.setState({text: ''});
    }
  };

  render() {
    return (
      <input
        type="text"
        placeholder="What you need to do?"
        onKeyPress={ this.handleKeyPressInput }
        onChange={ this.handleChangeInput }
        value={ this.state.text }
      />
    );
  }
}

AddTodo.propTypes = {
  onToDoAdd: PropTypes.func.isRequired,
};

export default AddTodo;