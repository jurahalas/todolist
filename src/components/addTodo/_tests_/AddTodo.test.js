import React from 'react';
import ReactDOM from 'react-dom';
import AddTodo from '../AddTodo';

it('renders without crashing', () => {
  ReactDOM.render(
    <AddTodo
      onToDoAdd={ jest.fn() }
    />, document.createElement('div'));
});